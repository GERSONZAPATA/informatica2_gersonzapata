/* Titulo:Denominacion de Billetes
 * Fecha: 22/08/2018
 * Autor: Gerson Zapata Agudelo
 * Descripcion: este programa entrega la denominacion minima de billetes para tener la cifra ingresada */
#include <iostream>
using namespace std;
int calculo_billetes(int array[],int monto);
int main()
{
    int billetes[10]={50000, 20000, 10000, 5000, 2000, 1000, 500, 200, 100, 50};
    int monto,faltante;
    cout << "Este programa muestra la denominacion minima de billetes" << endl;
    cout << endl;
    cout << "Ingrese el monto a calcular: ";
    cin >> monto;
    faltante=calculo_billetes(billetes,monto);
    cout << endl;
    cout << "Faltante es: " << faltante << endl;
    cout << endl;
    cout << "Fin del programa" << endl;
    return 0;
}
int calculo_billetes(int array[],int monto){
    for (int cont=0; cont<10; cont++){
        int cantidad=monto/array[cont];
        monto=monto%array[cont];
        cout << array[cont] << ":" << cantidad <<endl;
    }
    return monto;
}
