/* Titulo:Quiz 0
 * Fecha: 09/08/2017
 * Autor: Gerson Zapata Agudelo
 * Descripcion: Serie de Numero */
#include <iostream>
using namespace std;
int main()
{
    cout << "Este programa imprime una serie y calcula su promedio " << endl;
    cout << endl;
    int numserie, prom=1,sum=0;
    float total;
    cout << "Ingrese el numero de elementos a visualizar:";
    cin >> numserie;
    cout << endl;
    cout << "la serie es: ";
    for (int serie=0;serie < numserie;serie++){
        prom+=serie;
        cout << prom << ",";
        sum+=prom;
    }
    cout << endl;
    total=sum/numserie;
    cout << "el promedio de la serie es: " << total << endl;
    cout << "Fin del programa" << endl;
    return 0;
}

