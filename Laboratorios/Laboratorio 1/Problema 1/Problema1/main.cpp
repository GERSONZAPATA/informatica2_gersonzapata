/* Titulo:Tipo de letra
 * Fecha: 17/08/2018
 * Autor: Gerson Zapata Agudelo
 * Descripcion: este programa entrega si un caracter es un letra consonante o vocal o no es letra */
#include <iostream>
using namespace std;
bool Tipoletra(char* array, char val);
int main()
{
    char letra;
    char vocales[11]={'A','E','I','O','U','a','e','i','o','u',0};
    cout << "Este programa entrega si un caracter es un letra consonante o vocal o no es letra" << endl;
    cout << endl;
    cout << "ingrese un caracter: ";
    cin>> letra;
    if((letra>='A' && letra<='Z')|| (letra>= 'a' && letra<='z'))
    {
        if(Tipoletra(vocales,letra))
            cout << letra <<" es una vocal"<< endl;
        else
            cout << letra <<" es una consonante"<< endl;
    }
    else cout << letra << " no es una letra"<< endl;
    cout << endl;
    cout << "Fin del programa" << endl;
    return 0;
}
bool Tipoletra(char* array, char val)
{
    for(int cont=0; array[cont]!=0; cont++)
    {
        if(array[cont]== val)
            return true;
     }
    return false;
}
