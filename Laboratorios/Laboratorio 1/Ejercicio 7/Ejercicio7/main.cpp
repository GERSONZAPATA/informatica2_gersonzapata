/* Titulo:Suma de 0 a N
 * Fecha: 10/08/2018
 * Autor: Gerson Zapata Agudelo
 * Descripcion: este programa entrega la suma de todos los numeros desde 0 hasta N incluyendolo */
#include <iostream>
using namespace std;
int main()
{
    float num,sum,result=0;
    cout << "Este programa entrega la suma de todos los numeros anteriores y el mismo" << endl;
    cout << endl;
    cout << "Ingrese un numero: ";
    cin >> num;
    for (sum=1;sum<=num;sum++){
        result=sum+result;
    }
    cout << "La suma es:" << result << endl;
    cout << endl;
    cout << "Fin del programa" << endl;
    return 0;
}
