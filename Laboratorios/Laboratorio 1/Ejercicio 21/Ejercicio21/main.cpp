/* Titulo:Cambio de letras
 * Fecha: 13/08/2018
 * Autor: Gerson Zapata Agudelo
 * Descripcion: este programa cambia de minusculas a mayusculas y de mayusculas a minusculas */
#include <iostream>
using namespace std;
int main () {
    char letra;
    cout << "Este programa cambia de minusculas a MAYUSCULAS y viceversa" << endl;
    cout << endl;
    cout << "Ingrese una letra: ";
    cin >> letra;
    if((letra >= 'a') & (letra<='z')) {
        letra -= 'a' - 'A';
        cout << "letra convertida: " << letra << endl;
    }else if((letra >= 'A') & (letra<='Z')){
        letra -= 'A' - 'a';
        cout << "letra convertida: " << letra << endl;
    }
    cout << endl;
    cout << "Fin del programa" << endl;
    return 0;
}
