/* Titulo:suma de numeros
 * Fecha: 10/08/2018
 * Autor: Gerson Zapata Agudelo
 * Descripcion: este programa entrega la suma de todos los numeros ingresados hasta que el usuario ingrese el 0 */
#include <iostream>
using namespace std;
int main()
{
    int num=1,sum=0;
    cout << "Este programa suma todos los numeros ingresados hasta ingresar el 0" << endl;
    cout << endl;
    while (num!=0){
        cout << "Ingrese un numero: ";
        cin >> num;
        sum=sum+num;
    }
    cout << "la suma es:" << sum << endl;
    cout << endl;
    cout << "Fin del programa" << endl;
    return 0;
}
