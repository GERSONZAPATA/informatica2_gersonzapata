/* Titulo:Suma de primos menores
 * Fecha: 21/08/2018
 * Autor: Gerson Zapata Agudelo
 * Descripcion: Este programa entrega la suma de los numeros primos menores al numero ingresado */
#include <iostream>
using namespace std;
int main()
{
    int num,sum=0,cant=10;
    cout << "Este programa entrega la suma de los numeros primos menores al numero ingresado" << endl;
    cout << endl;
    cout << "Ingrese un numero: ";
    cin >> num;
    cant=cant*num;
    for (int cont=2;cont<=cant;cont++){
        while (num>cont){
            int prim=0;
            for(int mod=1;mod<=cant;mod++){
                if(cont%mod==0) prim++;
            }
            if (prim==2) sum=sum+cont;
            break;
        }
    }
    cout << "el resultado de la suma es: " << sum  << endl;
    cout << endl;
    cout << "Fin del programa" << endl;
    return 0;
}

