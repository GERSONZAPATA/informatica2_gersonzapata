/* Titulo:Piramide asteriscos
 * Fecha: 20/08/2018
 * Autor: Gerson Zapata Agudelo
 * Descripcion: este programa imprime una piramide de asteriscos */
#include <iostream>
using namespace std;
void impresion(int cantidad, char caracter);
int main()
{
    int impar = 0;
    cout << "Este programa entrega una priamide de acuerdo a un numero impar " << endl;
    cout << endl;
    cout << "Ingrese un numero impar: ";
    cin >> impar;
    while(impar%2==0){
        cout << "El numero " << impar << " no es impar" << endl;
        cout << "por favor ingrese un numero impar" << endl;
        cin >> impar;
    }
    int espacios = impar/2, asteriscos = 1;
    for(int super=0;super<((impar/2)+1);super++){
        impresion(espacios,' ');
        impresion(asteriscos,'*');
        cout << endl;
        espacios-=1;
        asteriscos+=2;
    }
    asteriscos=impar-2;
    for(int infer=0;infer<impar/2;infer++){
        impresion(espacios+2,' ');
        impresion(asteriscos,'*');
        cout << endl;
        espacios+=1;
        asteriscos-=2;
    }
    cout << endl;
    cout << "Fin del programa" << endl;
    return 0;
}
void impresion(int cantidad, char caracter){
    for(int imp=0;imp<cantidad;imp++){
        cout << caracter;
    }
}
