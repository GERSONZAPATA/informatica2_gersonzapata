/* Titulo:Numero mayor
 * Fecha: 10/08/2018
 * Autor: Gerson Zapata Agudelo
 * Descripcion: este programa entrega numero mayor de todos los numeros ingresados hasta que el usuario ingrese el 0 */
#include <iostream>
using namespace std;
int main()
{
    int num,mayor=0;
    cout << "Este programa muestra el mayor todos los numeros ingresados hasta ingresar el 0" << endl;
    cout << endl;
    while (num!=0){
        cout << "Ingrese un numero: ";
        cin >> num;
        if (mayor<num){
            mayor=num;
        }
    }
    cout << "el mayor es:" << mayor << endl;
    cout << endl;
    cout << "Fin del programa" << endl;
    return 0;
}
