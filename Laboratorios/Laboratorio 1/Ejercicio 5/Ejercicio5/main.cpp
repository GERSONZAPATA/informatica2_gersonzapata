/* Titulo:Division con Redondeo
 * Fecha: 09/08/2018
 * Autor: Gerson Zapata Agudelo
 * Descripcion: este programa divide dos numeros y redondea el resultado */
#include <iostream>
using namespace std;
int main()
{
    float num1,num2,result,redon;
    int ente;
    cout << "Este programa entrega la division redondeada" << endl;
    cout << endl;
    cout << "Ingrese un numero: ";
    cin >> num1;
    cout << "Ingrese otro numero: ";
    cin >> num2;
    if (num2==0){
        cout << "la division por cero no esta definida" << endl;
    } else {
        ente=(int (num1)/int (num2));
        redon=(num1/num2)-ente;
        if (redon>=0.5){
            result=ente+1;
        }
        else{
            result=ente;
        }
        cout << "el redondeo de la division " << num1 << "/" << num2 << " es:" << result << endl;
    }
    cout << endl;
    cout << "Fin del programa" << endl;
    return 0;
}
