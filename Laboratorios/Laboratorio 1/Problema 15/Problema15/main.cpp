/* Titulo:Suma diagonal espiral
 * Fecha: 22/08/2018
 * Autor: Gerson Zapata Agudelo
 * Descripcion: Este programa recibe un numero n y calcula la suma de
 * los numeros de las diagonales de una espiral nxn. */
#include <iostream>
using namespace std;
int main()
{
    int diag=1,salto1=2,impar,veces=0,salto2=2,suma=0;
    cout << "Este programa entrega la suma de la diagonal de una espiral "
            "del numero*el mismo numero ingresado" << endl;
    cout << endl;
    cout << "Ingrese un numero impar: ";
    cin >> impar;
    veces=impar/2;
    for (int fila=0;fila<veces;fila++){
        for (int inter=0;inter<4;inter++){
            suma=suma+(diag+salto1);
            diag=diag+salto2;
        }
        salto1=salto1+2;
        salto2=salto2+2;
    }
    suma=suma+1;
    cout << "En una espiral de " << impar << "x" << impar << ", la suma es: " << suma
         << endl;
    cout << endl;
    cout << "Fin del programa" << endl;
    return 0;
}
