/* Titulo:El mayor de dos numeros
 * Fecha: 09/08/2018
 * Autor: Gerson Zapata Agudelo
 * Descripcion: este programa compara dos numeros y determina cual es el mayor de los dos */
#include <iostream>
using namespace std;
int main()
{
    int num1,num2;
    cout << "Este programa entrega el mayor de dos numeros" << endl;
    cout << endl;
    cout << "Ingrese un numero: ";
    cin >> num1;
    cout << "Ingrese otro numero: ";
    cin >> num2;
    if (num1==num2){
           cout << num1 << " y " << num2 <<" son iguales" << endl;
    }else if (num1>num2){
        cout <<"El mayor es:" << num1 << endl;
    } else {
        cout << "El mayor es:" << num2 << endl;
    }
    cout << endl;
    cout << "Fin del programa" << endl;
    return 0;
}

