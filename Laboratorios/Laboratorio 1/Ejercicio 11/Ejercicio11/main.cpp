/* Titulo:Tabla de multiplicar
 * Fecha: 10/08/2018
 * Autor: Gerson Zapata Agudelo
 * Descripcion: Este programa entrega la tabla multiplicar de un numero */
#include <iostream>
using namespace std;
int main()
{
    int num,mult,resul;
    cout << "Este programa entrega la tabla multiplicar de un numero" << endl;
    cout << endl;
    cout << "Ingrese un numero: ";
    cin >> num;
    cout << "la tabla de multiplicar del "<< endl;
    for (mult=1;mult<=10;mult++){
        resul=mult*num;
        cout << mult << "x"<< num << "=" << resul << endl;
    }
    cout << endl;
    cout << "Fin del programa" << endl;
    return 0;
}
