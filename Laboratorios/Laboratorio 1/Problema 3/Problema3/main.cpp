/* Titulo: Validacion de fechas
 * Fecha: 17/08/2018
 * Autor: Gerson Zapata Agudelo
 * Descripcion: este programa entrega si una fechas es valida o no */
#include <iostream>
using namespace std;
int main()
{
    cout << "Este programa entrega si una fechas es valida o no" << endl;
    cout << endl;
    int mes,dia,mes31[7]={1,3,5,7,8,10,12},mes30[4]={4,6,9,11};
    bool n=false;
    cout << "ingrese el mes: ";
    cin >> mes;
    if (mes<=12){
        cout << "ingrese el dia: ";
        cin >> dia;
        if (mes==2){
            if (dia<30){
                if (dia==29){
                    cout<<dia<<"/"<<mes<<" es valida en bisiesto"<<endl;
                }else{
                    cout << dia << "/" << mes << " Es una fecha valida" << endl;
                }
            }else {
                cout << dia << "/" << mes << " Es una fecha invalida" << endl;
            }
        }else {
            //meses de 30
            for (int cont=0;cont<4;cont++){
                if (mes30[cont]==mes){
                    if (dia<=30){
                        n=true;
                    }
                }
            }
            //meses de 31
            for (int cont=0;cont<7;cont++){
                if (mes31[cont]==mes){
                    if (dia<=31){
                        n=true;
                    }
                }
            }
            if (n==true){
                cout<<dia<<"/"<<mes<<" Es una fecha valida"<<endl;
            }else {
                cout<<dia<<"/"<<mes<<" Es una fecha invalida"<<endl;
            }
        }
    }else{
        cout<<mes<<" es un mes invalido"<< endl;
    }
    cout << endl;
    cout << "Fin del programa" << endl;
    return 0;
}
