/* Titulo:Digitos de un numero
 * Fecha: 16/08/2018
 * Autor: Gerson Zapata Agudelo
 * Descripcion: Calculadora de suma,resta,multiplicacion y division */
#include <iostream>
using namespace std;
int main()
{
    cout << "Este programa es una Calculadora de suma,resta,multiplicacion y division" << endl;
    cout << endl;
    int num1,num2,resul=0;
    char sig;
    cout << "Ingrese el primer numero: " ;
    cin >> num1;
    cout << "Ingrese la operacion: " ;
    cin >> sig;
    cout << "Ingrese el segundo numero: " ;
    cin >> num2;
    if (sig>=42 && sig<=47){
        if (sig !=44 && sig !=46){
            switch (sig){
                case 42:resul=num1*num2;
                    break;
                case 43:resul=num1+num2;
                    break;
                case 45:resul=num1-num2;
                    break;
                case 47:resul=num1/num2;
                    break;
            }
            cout << "el resultado de "<<num1<<sig<<num2<< " es: " << resul<< endl;
        }
        else
            cout << sig << " no es una operacion valida" << endl;
    }
    else
        cout << sig << " no es una operacion valida" << endl;
    cout << endl;
    cout << "Fin del programa" << endl;
    return 0;
}
