/* Titulo:Digitos de un numero
 * Fecha: 14/08/2018
 * Autor: Gerson Zapata Agudelo
 * Descripcion: este programa entrega la cantidad de digitos de un numero */
#include <iostream>
using namespace std;
int main()
{
    int num,mod,dig=0,ori;
    cout << "Este programa determina la cantidad de digitos de un numero" << endl;
    cout << endl;
    cout << "Ingrese un numero: ";
    cin >> num;
    ori=num;
    while (num>0){
        mod=num%10;
        num=num/10;
        dig++;
    }
    cout << ori << " Tiene " << dig << " digitos" << endl;
    cout << endl;
    cout << "Fin del programa" << endl;
    return 0;
}
