/* Titulo:Ejercicio 1
 * Fecha: 09/08/2018
 * Autor: Gerson Zapata Agudelo
 * Descripcion: Residuo de dos numeros */
#include <iostream>
using namespace std;
int main()
{
    int num1, num2;
    float result=0;
    cout << "Este programa imprime el residuo entre dos numeros " << endl;
    cout << endl;
    cout << "Ingrese el primer numero: ";
    cin >> num1;
    cout << "Ingrese el segundo numero: ";
    cin >> num2;
    cout << endl;
    result= (float)num1/num2;
    cout << "el resuido de los dos numero es: " << result << endl;
    cout << endl;
    cout << "Fin del programa" << endl;
    return 0;
}
