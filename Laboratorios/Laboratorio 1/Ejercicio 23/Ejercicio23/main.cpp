/* Titulo:Minumo comun multiplo
 * Fecha: 14/08/2018
 * Autor: Gerson Zapata Agudelo
 * Descripcion: este programa entrega el minimo como un multiplo de dos numeros */
#include <iostream>
using namespace std;
int main () {
    int num1,num2,mcm=0;
    cout << "Este programa entrega el minimo como un multiplo de dos numeros" << endl;
    cout << endl;
    cout << "ingrese un numero: " ;
    cin >> num1;
    cout << "ingrese otro numero: ";
    cin >> num2;
    if (num1<num2)mcm=num2;
    else mcm=num1;
    while ((mcm%num1)!=0||(mcm%num2)!=0)mcm++;
    cout << "El minimo comun multiplo entre los dos numeros es: " << mcm << endl;
    cout << endl;
    cout << "Fin del programa" << endl;
    return 0;
}
