/* Titulo:Perimetro y Area de un cirulo
 * Fecha: 10/08/2018
 * Autor: Gerson Zapata Agudelo
 * Descripcion: este programa entrega el perimetro y el area de un circulo */
#include <iostream>
using namespace std;
int main()
{
    float radio,perimetro,area;
    cout << "Este programa entrega el perimetro y el area de un circulo" << endl;
    cout << endl;
    cout << "Ingrese el radio: ";
    cin >> radio;
    perimetro=(2*3.1416)*radio;
    area=3.1416*(radio*radio);
    cout << "el perimetro es de: "<< perimetro << endl;
    cout << "el area es de: "<< area << endl;
    cout << endl;
    cout << "Fin del programa" << endl;
    return 0;
}
