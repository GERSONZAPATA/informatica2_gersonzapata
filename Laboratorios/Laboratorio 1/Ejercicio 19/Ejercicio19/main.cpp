/* Titulo:Numeros primos
 * Fecha: 13/08/2018
 * Autor: Gerson Zapata Agudelo
 * Descripcion: este programa entrega si un numero es primo o no es primo */
#include <iostream>
using namespace std;
int main()
{
    int num,prim=0;
    cout << "Este programa determina si un numero es primo o no" << endl;
    cout << endl;
    cout << "Ingrese un numero: ";
    cin >> num;
    cout << endl;
    for (int entero=1;entero<num;entero++){
        if ((num%entero)==0){
            prim++;
        }
    }
    if (prim<=2){
        cout << num << " es un numero primo" << endl;
    }else {
        cout << num << " NO es un numero primo" << endl;
    }
    cout << endl;
    cout << "Fin del programa" << endl;
    return 0;
}
