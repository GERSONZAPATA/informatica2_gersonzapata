/* Titulo:punto 2
 * Fecha: 23/08/2018
 * Autor: Gerson Zapata Agudelo
 * Descripcion: notas curso */
#include <iostream>
using namespace std;
int main()
{
    int opc,est=0,lab=0,win=0;
    float sumcur=0,promcur,winner;
    cout << "Este programa para calcular notas del curso Informatica " << endl;
    cout << endl;
    while (opc!=6){
        cout << endl;
        cout << "-------------------------------------------------" << endl;
        cout << "MENU" << endl;
        cout << "1.Ingresar cantidad de estudiantes" << endl;
        cout << "2.Ingresar cantidad de laboratorios" << endl;
        cout << "3.Ingresar notas" << endl;
        cout << "4.Mostrar Promedio del curso" << endl;
        cout << "5.Mostrar Porcentaje de estudiantes que ganaron" << endl;
        cout << "6.Salir" << endl;
        cout << "-------------------------------------------------" << endl;
        cout << endl;
        cout << "seleccione una opcion: ";
        cin >> opc;
        switch (opc) {
                case 1:
                   cout << "ingrese la cantidad de estudiantes: ";
                   cin >> est;
                   break;
                case 2:
                    cout << "ingrese la cantidad de laboratorios: ";
                    cin >> lab;
                    break;
                case 3:
                for (int econt=1;econt<=est;econt++){
                            float par=0,par1,par2,par3,suml=0,proml=0,sumt=0,nlab;
                            cout << "ingrese las notas del estudiante "<< econt << endl;
                            cout << "parcial 1: ";
                            cin >> par1;
                            cout << "parcial 2: ";
                            cin >> par2;
                            cout << "parcial 3: ";
                            cin >> par3;
                            for (int lcont=1;lcont<=lab;lcont++){
                                cout << "laboratorio " << lcont <<": ";
                                cin >> nlab;
                                suml=suml+nlab;
                            }
                            par1=par1*0.1;
                            par2=par2*0.15;
                            par3=par3*0.2;
                            par=par1+par2+par3;
                            proml=(suml/lab)*0.55;
                            sumt=par+proml;
                            cout << "la definitiva del estudiante "<< econt << " es: " << sumt << endl;
                            sumcur=sumcur+sumt;
                            if (sumt>=3.0) win++;
                           }
                           break;
                case 4:
                   promcur=sumcur/est;
                   cout << "el promedio del curso es:" << promcur << endl;
                   break;
                case 5:
                   winner=win*(100/est);
                   cout << "el porcentaje de estudiantes que ganaron es: " << winner << "%" << endl;
                   break;
                default:
                    break;
        }
    }
    cout << endl;
    cout << "Fin del programa" << endl;
    return 0;
}
