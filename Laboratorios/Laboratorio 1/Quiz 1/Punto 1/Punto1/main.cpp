/* Titulo:punto 1
 * Fecha: 23/08/2018
 * Autor: Gerson Zapata Agudelo
 * Descripcion: Impresion de + y * */
#include <iostream>
using namespace std;
int main()
{
    int impar,imp1,imp2=1;
    cout << "Este programa imprime + y * respecto a un numero impar " << endl;
    cout << endl;
    cout << "Ingrese un numero impar: " ;
    cin >> impar;
    imp1=impar;
    for (int lin=1;lin<=impar;lin++){
        for(int sum=imp1;sum>0;sum--){
            cout << "+";
        }
        for(int ast=1;ast<=imp2;ast++){
            cout << "*";
        }
        imp1--;
        imp2++;
        cout << endl;
    }
    cout << endl;
    cout << "Fin del programa" << endl;
    return 0;
}
