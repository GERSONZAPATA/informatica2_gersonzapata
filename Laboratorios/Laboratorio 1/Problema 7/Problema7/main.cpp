/* Titulo:Pares de Fibonacci
 * Fecha: 20/08/2018
 * Autor: Gerson Zapata Agudelo
 * Descripcion: este programa entrega la suma de los numeros pares inferores en la serie de
 * fibonacci del numero ingresado */
#include <iostream>
using namespace std;
int main()
{
    int num,actu=1,ante=1,sum=0,alma;
    cout << "Este programa entrega la suma de los numeros pares de fibonacci "
            "apartir de un numero ingresado " << endl;
    cout << endl;
    cout << "ingrese numero: ";
    cin >> num;
    for(int i=1;actu<num;i++){
        if (actu%2==0)sum=sum+actu;
        alma=ante;
        actu=actu+ante;
        ante=actu;
        actu=alma;
    }
    cout << "la suma es:" << sum << endl;
    cout << endl;
    cout << "Fin del programa" << endl;
    return 0;
}
