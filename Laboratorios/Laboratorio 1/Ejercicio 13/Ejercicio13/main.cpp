/* Titulo:los divisiores
 * Fecha: 10/08/2018
 * Autor: Gerson Zapata Agudelo
 * Descripcion: este programa entrega los divisiores de un numero */
#include <iostream>
using namespace std;
int main()
{
    int num,divi,mod;
    cout << "Este programa entrega los divisores de un numero" << endl;
    cout << endl;
    cout << "Ingrese un numero: ";
    cin >> num;
    cout << "Los divisiores de "<< num <<" son:" << endl;
    for (divi=1;divi<=num;divi++){
        mod=num%divi;
        if (mod==0){
            cout << divi << endl;
        }
    }
    cout << endl;
    cout << "Fin del programa" << endl;
    return 0;
}
