/* Titulo:Digitos elevados
 * Fecha: 21/08/2018
 * Autor: Gerson Zapata Agudelo
 * Descripcion: este programa entrega la suma de los digitos de un numero
 * elevados por si mismos */
#include <iostream>
using namespace std;
int main()
{
    int num,sum=0;
    cout << "Este programa suma los digitos elevados por si mismos de "
            "un numero" << endl;
    cout << endl;
    cout << "Ingrese un numero: ";
    cin >> num;
    while (num>0){
        int dig=num%10;
        num = num/10;
        int poten=dig,elev=dig;
        for (int cont=1;cont<=dig;cont++){
            if (cont==1){
                elev=poten;
            }else{
                elev=elev*poten;
            }
        }
        sum=sum+elev;
    }
    cout << "El resultado de la suma es: " << sum << endl;
    cout << endl;
    cout << "Fin del programa" << endl;
    return 0;
}
