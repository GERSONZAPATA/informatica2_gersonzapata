/* Titulo:Divisiores de numeros triangulares
 * Fecha: 23/08/2018
 * Autor: Gerson Zapata Agudelo
 * Descripcion: este programa el primer numero triangular con mas divisiores respecto
 * al numero ingresado */
#include <iostream>
using namespace std;
int main()
{
    int num,mayor=0,lim=10,numf=0,div;
    cout << "Este programa entrega el primer numero triangular con mas divisores a un "
            "numero " << endl;
    cout << endl;
    cout << "ingrese cuantos divisores desea: " ;
    cin >> num;
    lim=num*lim;
    for(int cont=1;mayor<lim;cont++){
        int acum=0;
        mayor=mayor+cont;
        if (mayor>=lim)break;
        for(int mod=1;mod<=lim;mod++){
            if(mayor%mod==0) acum++;
        }
        if (acum>num) {
            numf=mayor;
            div=acum;
            break;
        }
    }
    cout << "el numero es: " << numf  << " que tiene " << div << " divisores"<< endl;
    cout << endl;
    cout << "Fin del programa" << endl;
    return 0;
}
