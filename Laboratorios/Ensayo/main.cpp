#include <iostream>
using namespace std;
int main()
{
    float num1, num2,sum,res,mul,div;
    cout << "Este programa muestra las operaciones basicas entre dos numeros" << endl;
    cout << endl;
    cout << "ingrese el primer numero:" ;
    cin >> num1;
    cout << "ingrese el segundo numero:" ;
    cin >> num2;
    sum = num1 + num2;
    res = num1 - num2;
    mul = num1 * num2;
    div = num1 / num2;
    cout << endl;
    cout << "la suma es: " << sum << endl;
    cout << "la resta es: " << res << endl;
    cout << "la multiplicacion es: " << mul << endl;
    cout << "la division es: " << div << endl;
    cout << "Fin del programa" << endl;
    return 0;
}
